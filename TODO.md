# TODO list

- Add toybox for x86_64
- Look at adding prebuilt voice recognition for Velvet
- Update sigma gapps from a proper device release
- Test/fix flashing zip on sigma ROMs
- Test/fix gapps issues on sigma ROMs
- CI/CD for privapp-permissions
